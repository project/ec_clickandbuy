Payment Module  : ec_ClickandBuy
Original Author : Dublin Drupaller
Settings        : administer > eCommerce configuration > receipt types > ClickandBuy

********************************************************************
DESCRIPTION:

This module allows you to accept payments to your site using the ClickandBuy payment processing server
Designed to work with Drupal EC4 For Drupal 6.x

Contact Dublin Drupaller via http://www.DublinDrupaller.com for assistance
or for suggestions, ideas or improvements.
********************************************************************




INSTALLATION
------------------
This version of the ec_clickandbuy.module requires version 6.x-4.x of the Drupal eCommerce API

Pre-installation note: After downloading the module from Drupal.org, it is recommended you
upload the ec_ClickandBuy module files to /sites/all/modules/ecommerce/ec_clickandbuy

(a) Enable the module as you would any other Drupal module under ADMINISTER -> SITE BUILDING -> MODULES

(b) Once enabled go to the ec_ClickandBuy settings page which can be found by following these links

    ADMINISTER > E-COMMERCE CONFIGURATION -> RECEIPT TYPES ->

(c) Click on the ClickandBuy link to display the OPTIONS page and then click on the SETTINGS tab

(d) Enter in your details as provided by ClickandBuy when you setup your account.

(e) Once you have saved your ClickandBuy settings, click on the OPTIONS link to display the OPTIONS page and ensure the ALLOW PAYMENTS
option is enabled.


UNINSTALL
-------------

An uninstall routine is included with the ClickandBuy module.

(a) Go to ADMINISTER -> SITE BUILDING -> MODULES, uncheck the ClickandBuy module checkbox and save configuration.
(b) Click on the UNINSTALL link at the top of the MODULES page and follow the instructions to remove all references to ClickandBuy
in the database. (Note: This does not remove ClickandBuy transactions from the eCommerce database tables)
(c) Remove the ec_clickandbuy files and folder from your server.


DEVELOPER NOTES
----------------
Here are a list of key variables for reference purposes

1st Confirmation stage variables (ec_clickandbuy_check function):

$_SERVER["HTTP_X_PRICE"];  // ClickandBuy Price recorded in Millicents
$_SERVER["HTTP_X_USERID"]; // ClickandBuy Customer Reference Number
$_SERVER["HTTP_X_TRANSACTION"];		// ClickandBuy unique Transaction Reference Number
$_SERVER["HTTP_X_CURRENCY"];			// ClickandBuy recorded transaction currency
$_SERVER["REMOTE_ADDR"];			// IP Address of the ClickandBuy Web Proxy
$_GET["externalBDRID"];	  		// Your unique transaction ID , this parameter is mandatory

TO DO's
--------
ClickandBuy Second Confirmation integration
ClickandBuy Event Messaging Service (EMS) integration into the main Drupal eCommerce Admin.
Make variables available to the thank you page so the site admin can output a nicer message.

NOTES
---------
For support/assistance, or if you have any ideas for improvements, please
contact Dublin Drupaller: dub@dublindrupaller.com
http://www.DublinDrupaller.com

