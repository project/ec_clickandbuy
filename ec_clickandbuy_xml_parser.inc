<?php
/**
 * ClickandBuy XML Parser Class 
 * 
 * Parses the ClickandBuy XML data into an object structure much like the SimpleXML extension.
 *
 * @author Dublin Drupaller <dub@dublindrupaller.com>
*/
class XMLParser {
    var $parser;
    var $xml;
    var $document;
    var $stack;
    var $cleanTagNames;

    function XMLParser($xml = '', $cleanTagNames = true) {
      $this->xml = $xml;
      $this->stack = array();
      $this->cleanTagNames = $cleanTagNames;
      }

    function Parse() {
      $this->parser = xml_parser_create();
      xml_set_object($this->parser, $this);
      xml_set_element_handler($this->parser, 'StartElement', 'EndElement');
      xml_set_character_data_handler($this->parser, 'CharacterData');
      if (!xml_parse($this->parser, $this->xml))
        $this->HandleError(xml_get_error_code($this->parser), xml_get_current_line_number($this->parser), xml_get_current_column_number($this->parser));
        xml_parser_free($this->parser);
      }

    function HandleError($code, $line, $col) {
      trigger_error('XML Parsing Error at '.$line.':'.$col.'. Error '.$code.': '.xml_error_string($code));
    }

    function GenerateXML() {
      return $this->document->GetXML();
    }

    function GetStackLocation() {
      $return = '';
      foreach($this->stack as $stack)
        $return .= $stack.'->';
        return rtrim($return, '->');
      }

    function StartElement($parser, $name, $attrs = array()) {
      $name = strtolower($name);
      if (count($this->stack) == 0) {
        $this->document = new XMLTag($name, $attrs);
        $this->stack = array('document');
        }
      else {
        $parent = $this->GetStackLocation();
        eval('$this->'.$parent.'->AddChild($name, $attrs, '.count($this->stack).', $this->cleanTagNames);');
        if($this->cleanTagNames)
          $name = str_replace(array(':', '-'), '_', $name);
        eval('$this->stack[] = $name.\'[\'.(count($this->'.$parent.'->'.$name.') - 1).\']\';');
        }
      }

    function EndElement($parser, $name) {
      array_pop($this->stack);
      }

    function CharacterData($parser, $data) {
      $tag = $this->GetStackLocation();
      eval('$this->'.$tag.'->tagData .= trim($data);');
      }
}

class XMLTag {
    var $tagAttrs;
    var $tagName;
    var $tagData;
    var $tagChildren;
    var $tagParents;

    function XMLTag($name, $attrs = array(), $parents = 0){
      $this->tagAttrs = array_change_key_case($attrs, CASE_LOWER);
      $this->tagName = strtolower($name);
      $this->tagParents = $parents;
      $this->tagChildren = array();
      $this->tagData = '';
      }
    function AddChild($name, $attrs, $parents, $cleanTagName = true){
      if (in_array($name, array('tagChildren', 'tagAttrs', 'tagParents', 'tagData', 'tagName'))) {
        trigger_error('You have used a reserved name as the name of an XML tag. Please consult the documentation (http://www.criticaldevelopment.net/xml/) and rename the tag named "'.$name.'" to something other than a reserved name.', E_USER_ERROR);
        return;
        }
      $child = new XMLTag($name, $attrs, $parents);
      if($cleanTagName)
        $name = str_replace(array(':', '-'), '_', $name);
      elseif(strstr($name, ':') || strstr($name, '-'))
        trigger_error('Your tag named "'.$name.'" contains either a dash or a colon. Neither of these characters are friendly with PHP variable names, and, as such, they cannot be accessed and will cause the parser to not work. You must enable the cleanTagName feature (pass true as the second argument of the XMLParser constructor). For more details, see http://www.criticaldevelopment.net/xml/', E_USER_ERROR);
      if(!isset($this->$name))
        $this->$name = array();
        $this->{$name}[] =& $child;
        $this->tagChildren[] =& $child;
      }
    
    function GetXML(){
      $out = "\n".str_repeat("\t", $this->tagParents).'<'.$this->tagName;
      foreach($this->tagAttrs as $attr => $value)
      $out .= ' '.$attr.'="'.$value.'"';
      if(empty($this->tagChildren) && empty($this->tagData))
        $out .= " />";
      else {
        if(!empty($this->tagChildren)) {
          $out .= '>';
          foreach($this->tagChildren as $child) {
            if(is_object($child))
              $out .= $child->GetXML();
            }
          $out .= "\n".str_repeat("\t", $this->tagParents);
          }
          elseif(!empty($this->tagData))
            $out .= '>'.$this->tagData;
            $out .= '</'.$this->tagName.'>';
            }
          return $out;
      }
    
    function Delete($childName, $childIndex = 0) {
      $this->{$childName}[$childIndex]->DeleteChildren();
      $this->{$childName}[$childIndex] = null;
      unset($this->{$childName}[$childIndex]);
      for($x = 0; $x < count($this->tagChildren); $x ++) {
        if(is_null($this->tagChildren[$x]))
          unset($this->tagChildren[$x]);
        }
      }
    
    function DeleteChildren(){
      for($x = 0; $x < count($this->tagChildren); $x ++) {
        $this->tagChildren[$x]->DeleteChildren();
        $this->tagChildren[$x] = null;
        unset($this->tagChildren[$x]);
        }
      }
}